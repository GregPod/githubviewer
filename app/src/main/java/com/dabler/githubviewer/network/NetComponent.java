package com.dabler.githubviewer.network;

import com.dabler.githubviewer.AppModule;
import com.dabler.githubviewer.PresentersModule;
import com.dabler.githubviewer.repoDetails.RepoDetailsFragment;
import com.dabler.githubviewer.reposList.ReposListFragment;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, NetModule.class, PresentersModule.class})
public interface NetComponent {

    void inject(ReposListFragment activity);

    void inject(RepoDetailsFragment activity);
}