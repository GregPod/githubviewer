package com.dabler.githubviewer.network;

import com.dabler.githubviewer.model.Repository;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface GithubService {

    @GET("users/{user}/repos")
    Observable<List<Repository>> getRepos(@Path("user") String user, @Query("per_page") int perPage);
}
