package com.dabler.githubviewer.repoDetails;

import com.dabler.githubviewer.model.Repository;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class RepoDetailsPresenter extends MvpBasePresenter<RepoDetailsInterfaces.View>{

    private Repository repository;

    public RepoDetailsPresenter(){
        //no-op
    }

    public void setRepository(Repository repository){
        this.repository = repository;
    }

    public void updateData(Locale locale) {
        try {
            getView().initializeView(repository.getName(), repository.getLanguage(), reformatDate(repository.getCreatedAt(), locale), repository.getDescription());
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public String reformatDate(String stringDate, Locale locale) throws ParseException {
        SimpleDateFormat initialFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
        SimpleDateFormat finalFormat = new SimpleDateFormat("dd/MM/yyyy", locale);
        try {
            Date date = initialFormat.parse(stringDate);
            return finalFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
