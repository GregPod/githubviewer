package com.dabler.githubviewer.repoDetails;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dabler.githubviewer.App;
import com.dabler.githubviewer.R;
import com.dabler.githubviewer.model.Repository;
import com.hannesdorfmann.mosby3.mvp.MvpFragment;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RepoDetailsFragment extends MvpFragment<RepoDetailsInterfaces.View, RepoDetailsPresenter>
        implements RepoDetailsInterfaces.View {

    private static final String REPOSITORY_PARM = "repository_param";

    @BindView(R.id.name_text_view)
    TextView nameTextView;

    @BindView(R.id.language_text_view)
    TextView languageTextView;

    @BindView(R.id.created_at_text_view)
    TextView createdAtTextView;

    @BindView(R.id.description_text_view)
    TextView description_at_text_view;

    @Inject
    RepoDetailsPresenter presenter;

    public RepoDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public RepoDetailsPresenter createPresenter() {
        if (getArguments() != null) {
            Repository repository = getArguments().getParcelable(REPOSITORY_PARM);
            presenter.setRepository(repository);
        }

        return presenter;
    }

    public static RepoDetailsFragment newInstance(Repository repository) {
        RepoDetailsFragment fragment = new RepoDetailsFragment();
        Bundle args = new Bundle();
        args.putParcelable(REPOSITORY_PARM, repository);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App) getActivity().getApplication()).getNetComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_repo_details, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.updateData(getResources().getConfiguration().locale);
    }


    @Override
    public void initializeView(String nameText, String languageText, String createdAtText, String descriptionText) {
        nameTextView.setText(nameText);
        languageTextView.setText(languageText);
        createdAtTextView.setText(createdAtText);
        description_at_text_view.setText(descriptionText);
    }
}
