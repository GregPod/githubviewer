package com.dabler.githubviewer.repoDetails;

import com.hannesdorfmann.mosby3.mvp.MvpView;

public interface RepoDetailsInterfaces {

    interface View extends MvpView {

        void initializeView(String nameText, String languageText, String createdAtText, String descriptionText);
    }
}
