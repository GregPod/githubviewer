package com.dabler.githubviewer.repoDetails;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.dabler.githubviewer.R;
import com.dabler.githubviewer.model.Repository;

public class RepoDetailsActivity extends AppCompatActivity {

    public static String REPO_DETAILS_INTENT_KEY = "repoDetails";

    public static void startActivity(Context context, Repository repository) {
        Intent intent = new Intent(context, RepoDetailsActivity.class);
        intent.putExtra(REPO_DETAILS_INTENT_KEY, repository);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repo_details);

        Repository repositoryExtra = getIntent().getParcelableExtra(REPO_DETAILS_INTENT_KEY);
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container, RepoDetailsFragment.newInstance(repositoryExtra))
                .commit();
    }
}
