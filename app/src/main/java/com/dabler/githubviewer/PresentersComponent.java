package com.dabler.githubviewer;

import com.dabler.githubviewer.network.NetModule;
import com.dabler.githubviewer.repoDetails.RepoDetailsFragment;
import com.dabler.githubviewer.reposList.ReposListFragment;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, NetModule.class, PresentersModule.class})
public interface PresentersComponent {

    void inject(ReposListFragment activity);

    void inject(RepoDetailsFragment activity);
}
