package com.dabler.githubviewer;

import android.app.Application;

import com.dabler.githubviewer.network.DaggerNetComponent;
import com.dabler.githubviewer.network.NetComponent;
import com.dabler.githubviewer.network.NetModule;

public class App extends Application{

    private NetComponent netComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        netComponent = DaggerNetComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule(BuildConfig.API_URL))
                .presentersModule(new PresentersModule())
                .build();
    }

    public NetComponent getNetComponent(){
        return netComponent;
    }
}
