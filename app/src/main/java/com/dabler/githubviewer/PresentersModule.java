package com.dabler.githubviewer;

import com.dabler.githubviewer.repoDetails.RepoDetailsPresenter;
import com.dabler.githubviewer.reposList.ReposListPresenter;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class PresentersModule {

    @Provides
    ReposListPresenter provideReposListPresenter(Retrofit retrofit) {
        return new ReposListPresenter(retrofit);
    }

    @Provides
    RepoDetailsPresenter provideRepoDetailsPresenter() {
        return new RepoDetailsPresenter();
    }
}
