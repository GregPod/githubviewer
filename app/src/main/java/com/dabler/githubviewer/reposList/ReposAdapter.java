package com.dabler.githubviewer.reposList;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dabler.githubviewer.R;
import com.dabler.githubviewer.model.Repository;

import java.util.ArrayList;
import java.util.List;

public class ReposAdapter extends RecyclerView.Adapter<ReposAdapter.ViewHolder> {
    private List<Repository> repos;
    private ReposListInterfaces.RecyclerItemClickListener clickListener;

    public ReposAdapter(ReposListInterfaces.RecyclerItemClickListener clickListener) {
        this.clickListener = clickListener;
        repos = new ArrayList<>();
    }

    void addRepos(List<Repository> repos) {
        this.repos = repos;
        this.notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                         int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.repo_card_view, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.textView.setText(repos.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return repos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textView;

        ViewHolder(View v) {
            super(v);
            textView = v.findViewById(R.id.repo_name);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            clickListener.onItemClickListener(repos.get(getLayoutPosition()));
        }
    }
}