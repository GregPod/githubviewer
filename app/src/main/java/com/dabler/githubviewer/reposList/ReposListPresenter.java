package com.dabler.githubviewer.reposList;

import com.dabler.githubviewer.model.Repository;
import com.dabler.githubviewer.network.GithubService;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class ReposListPresenter extends MvpBasePresenter<ReposListInterfaces.View> {

    private final Retrofit retrofit;
    private List<Repository> reposList;

    @Inject
    public ReposListPresenter(Retrofit retrofit){
        this.retrofit = retrofit;
    }

    public void addData(List<Repository> reposList){
        this.reposList = reposList;
    }

    void loadData() {
        retrofit.create(GithubService.class).getRepos("square", 20)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(repos -> {
                    if (getView() != null) {
                        reposList = repos;
                        getView().setData(repos);
                    }
                }, error -> {
                    if (getView() != null) {
                        getView().showError(error, false);
                    }
                });
    }

    void onRecyclerItemClick(Repository repository) {
        if (getView() != null)
            getView().openRepoDetailsActivity(repository);
    }

    public List<Repository> filterList(String query){
        return Observable.fromIterable(reposList).filter(item -> item.getName().toLowerCase().contains(query.toLowerCase())).toList().blockingGet();
    }

}
