package com.dabler.githubviewer.reposList;

import com.dabler.githubviewer.model.Repository;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

import java.util.List;

public interface ReposListInterfaces {

    interface View extends MvpLceView<List<Repository>> {
        void openRepoDetailsActivity(Repository repository);
    }

    interface RecyclerItemClickListener {
        void onItemClickListener(Repository repository);
    }
}
