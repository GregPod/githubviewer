package com.dabler.githubviewer.reposList;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.dabler.githubviewer.App;
import com.dabler.githubviewer.R;
import com.dabler.githubviewer.model.Repository;
import com.dabler.githubviewer.repoDetails.RepoDetailsActivity;
import com.hannesdorfmann.mosby3.mvp.MvpFragment;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReposListFragment extends MvpFragment<ReposListInterfaces.View, ReposListPresenter>
        implements ReposListInterfaces.View, ReposListInterfaces.RecyclerItemClickListener {

    @BindView(R.id.repos_recycler_view)
    RecyclerView reposRecyclerView;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.error_text_view)
    TextView errorTextView;

    @Inject
    ReposListPresenter presenter;

    ReposAdapter adapter;

    public ReposListFragment() {
        // Required empty public constructor
    }

    @Override
    public ReposListPresenter createPresenter() {
        return presenter;
    }

    public static ReposListFragment newInstance() {
        return new ReposListFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App) getActivity().getApplication()).getNetComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_repositories, container, false);
        ButterKnife.bind(this, view);
        initializeRecyclerView();

        loadData(false);
        return view;
    }

    @Override
    public void openRepoDetailsActivity(Repository repository) {
        RepoDetailsActivity.startActivity(getContext(), repository);
    }

    @Override
    public void onItemClickListener(Repository repository) {
        getPresenter().onRecyclerItemClick(repository);
    }

    @Override
    public void showLoading(boolean pullToRefresh) {
        //no-op
    }

    @Override
    public void showContent() {
        //no-op
    }

    @Override
    public void showError(Throwable e, boolean pullToRefresh) {
        errorTextView.setVisibility(View.VISIBLE);
    }

    @Override
    public void setData(List<Repository> data) {
        progressBar.setVisibility(View.GONE);
        adapter.addRepos(data);
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        errorTextView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        presenter.loadData();
    }

    private void initializeRecyclerView() {
        adapter = new ReposAdapter(this);
        reposRecyclerView.setHasFixedSize(true);
        reposRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        reposRecyclerView.setAdapter(adapter);
    }

    public void getSearchQuery(String query) {
        List<Repository> list = presenter.filterList(query);
        adapter.addRepos(list);
    }
}
