package com.dabler.githubviewer.reposList;

import android.app.SearchManager;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;

import com.dabler.githubviewer.R;
import com.jakewharton.rxbinding2.support.v7.widget.RxSearchView;

public class MainActivity extends AppCompatActivity {

    ReposListFragment reposListFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        reposListFragment = ReposListFragment.newInstance();
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container, reposListFragment)
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);

        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        RxSearchView
                .queryTextChangeEvents(searchView)
                .skipInitialValue()
                .subscribe(query -> reposListFragment.getSearchQuery(query.queryText().toString()),
                        error -> Log.e("Error", error.toString()));

        return true;
    }
}
