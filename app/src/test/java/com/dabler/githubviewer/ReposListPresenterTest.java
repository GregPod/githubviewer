package com.dabler.githubviewer;

import com.dabler.githubviewer.model.Repository;
import com.dabler.githubviewer.reposList.ReposListPresenter;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Retrofit;

import static org.junit.Assert.*;

public class ReposListPresenterTest {

    @Inject
    Retrofit retrofit;

    ReposListPresenter presenter;
    Repository[] reposList = {
            new Repository("aab"),
            new Repository("aabc"),
            new Repository("xxx")
    };

    @Before
    public void setUp() {
        this.presenter = new ReposListPresenter(null);
        this.presenter.addData(Arrays.asList(reposList));
    }

    @Test
    public void correctlyFiltersRepos() throws Exception {
        List<Repository> filteredList = presenter.filterList("a");
        assertEquals(filteredList.size(), 2);

        List<Repository> filteredList2 = presenter.filterList("z");
        assertEquals(filteredList2.size(), 0);
    }
}